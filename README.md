Test_cooling_schedule contain files for conducting tests on how different "cooling schedule" affect the convergence,
while Test_neighbours_mapping contain similar files for condcting tests on how different manner of selecting systems states variations change the convergence.
The run[...].sh script in each folder run the heuristics according to a given setting, given in [test[...].f95
TSP_SimAnn_module.f95 is the module containing all the nice objects function and methods used elsewhere.
Some introduction to the topic and discussion of my [incredible] results are given in the .pdf
Please try and add new temperature schedule, energy distribution or states mappings if it seems fun to do so.