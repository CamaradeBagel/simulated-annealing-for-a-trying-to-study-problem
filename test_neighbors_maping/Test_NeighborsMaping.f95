program Template_program
    
    !use mpi_f08
    use TSP_SimAnn_module
    implicit none
    ! For describing the TSP problem
    type(TSP_Problem):: Problem ! derived type defined in the module, describe a set of points and the distances
    integer, parameter:: N_pts=1000, n_dim=4 ! N points to find a loop on, in n dimensions
    real(dp), parameter:: d=1.0_dp ! dimension of the dxdx...xd euclidian box in which the points are distributed

    ! for making many tests with each cooling schedule
    integer, parameter :: NbTest=30 ! number of successive test optimizations to do with each cooling parameter
    integer :: whichTest ! index to iterate over successive tests

    ! For temperature schedule
    real(dp), parameter:: T0=5.0_dp , Tf = 0.0001_dp! initial and final temperature of the geometric schedule
    integer, parameter:: K_step=100000, nbNeighborMapings=3 ! number of temperature steps/iterations,
                                                         ! number of different NeighborMaping tested
    real(dp):: scheduleT(K_step) ! will contain the lists of temperatures to iterate on during
                                             ! annealing
    integer:: whichNeighborMaping ! to iterate over the different Neighbors maping

    ! for results of optimization, and initialization
    integer:: finalState(N_pts), bestState(N_pts), K_final, State0(N_pts), kMax_all(NbTest)
    !!integer, allocatable:: historyStates(:,:) ! we want to save the results (State,Energies) of every iteration
    real(dp):: finalEnergy, bestEnergy, finalEnergy_all(NbTest), bestEnergy_all(NbTest), &
                &finalEnergy_average, bestEnergy_average, finalEnergy_std, bestEnergy_std, &
                & kMax_average, kMax_std
    real(dp), allocatable:: historyEnergies(:)

    ! For saving results in files
    character(100):: EnHistFile!!, StaHistFile
    integer:: Time(8)

! IMPLEMENTING POINTER WOULD BE NICE, BUT I HAVE NO TIME TO FIGURE OUT HOW TO DO SO IN FORTRAN
!     abstract interface
!       subroutine neighborsMethod(Problem, State, trialState, deltaE)
!         ! return a neighboring solution from the current state using random neighbors maping.
!         ! either "vertex insert", "block reverse" or "switch"
!         type(TSP_Problem), intent(in):: Problem
!         integer, intent(in):: State(:) ! current state
!         integer, intent(out):: trialState(:) ! returned trial state (after vertex insert)
        
!       end subroutine neighborsMethod
!    end interface

!    pointer:: NeighborMethod_list(nbNeighborMethods)

!    procedure (neighborsMethod), pointer :: NeighborMethod_pointer=> null ()

!_________________________________________________________________________________end of allocation

    ! create TSP problem with N RANDOM points in a n dimensionnal EUCLIDIAN space, a dxdx...xd box
    Problem = randomPts_Euclidian(N_pts ,n_dim, d)
    write(*,*)" TSP Problem defined"
    ! Create a geometric cooling schedule of K_steps from T0 to Tf
    scheduleT = geometricScheduleT(T0, Tf, K_step)

    ! IMPLEMENTING POINTER WOULD BE NICE, BUT I HAVE NO TIME TO FIGURE OUT HOW TO DO SO IN FORTRAN
    ! ! create a pointer to the VERTEX INSERT neighbors maping
    ! NeighborMethod_list(1) => neighborMethod_totDist_vertexInsert
    ! ! create a pointer to the SYMETRIC BLOCK REVERSE neighbors maping
    ! NeighborMethod_list(2) => neighborMethod_totDist_blockReverse_symetric
    ! ! create a pointer to the SYMETRIC SWITCH maping
    ! NeighborMethod_list(3) => neighborMethod_totDist_switch_symetric
    ! write(*,*)" Neighbors mapings defined"
    
    ! create a common initial State/path to use for all the tests, so they have similar conditions
    State0 = randomState(N_pts)
    write(*,*)" Initial random state defined"

    ! Approximatly solve using simulated annealing
    ! We use sum of the distances as total cost, in this euclidian case we know that the distances are symetric.
    ! We sample trial states based on a Gibbs probability distribution of their energies.
    ! We vary the Method used to find nearest neighbors
    write(*,*)" ___Computation starts___"
    DO whichNeighborMaping = 1,nbNeighborMapings
        DO whichTest = 1,NbTest
            write(*,*)"-Neighbors Maping: ",whichNeighborMaping, ", test ", whichTest, "/", NbTest
            IF (whichNeighborMaping.EQ.1) THEN ! vertex insert
                CALL simulated_annealing(Problem, effHamil=effHamil_totDist, flipProbDist=flipProbDist_Gibbs, &
                                                &neighborMethod=neighborMethod_totDist_vertexInsert, &
                                                &scheduleT=scheduleT, &
                                                &finalState=finalState, finalEnergy=finalEnergy, &
                                                &bestState=bestState, bestEnergy=bestEnergy, &
                                                &historyEnergies=historyEnergies, initState=State0)
            ELSE IF (whichNeighborMaping.EQ.2) THEN !block reverse
                CALL simulated_annealing(Problem, effHamil=effHamil_totDist, flipProbDist=flipProbDist_Gibbs, &
                                                &neighborMethod=neighborMethod_totDist_blockReverse_symetric, &
                                                &scheduleT=scheduleT, &
                                                &finalState=finalState, finalEnergy=finalEnergy, &
                                                &bestState=bestState, bestEnergy=bestEnergy, &
                                                &historyEnergies=historyEnergies, initState=State0)
            ELSE ! switch
                CALL simulated_annealing(Problem, effHamil=effHamil_totDist, flipProbDist=flipProbDist_Gibbs, &
                                                &neighborMethod=neighborMethod_totDist_switch_symetric, &
                                                &scheduleT=scheduleT, &
                                                &finalState=finalState, finalEnergy=finalEnergy, &
                                                &bestState=bestState, bestEnergy=bestEnergy, &
                                                &historyEnergies=historyEnergies, initState=State0)
            END IF
            ! print some minimal info about the results
            K_final = SIZE(historyEnergies) 
            write(*,*)"    number of iterations before convergence: ", K_final
            write(*,*)"    Lowest distance:", bestEnergy
            write(*,*)"    Last distance:", historyEnergies(K_final)
            !Writing the results require filenames
            CALL date_and_time(VALUES=Time)
            write(EnHistFile,'(a,i0,a,i0,a,i0,a)') 'energyHistory_', whichNeighborMaping,"_", whichTest, &
                                                &"_",TIME(4),'.txt'
            
            ! save the results in files (~1/100 of the pts)
            CALL WriteResults(historyEnergies, EnHistFile, 100, scheduleT)


        ! we update the energies for averages , as well as k_max
        finalEnergy_all(whichTest) = finalEnergy
        bestEnergy_all(whichTest) = bestEnergy
        kMax_all(whichTest) = K_final
        END DO
        ! we compute the average best and final energy, as well as k_max, for this schedule
        finalEnergy_average = SUM(finalEnergy_all)/SIZE(finalEnergy_all,1)
        bestEnergy_average = SUM(bestEnergy_all)/SIZE(bestEnergy_all,1)
        kMax_average = SUM(kMax_all)/SIZE(kMax_all,1)
        ! we compute the standard deviation of best and final energies, as well as k_max, for this schedule
        finalEnergy_std = SQRT(SUM((finalEnergy_all-finalEnergy_average)**2)/SIZE(finalEnergy_all,1))
        bestEnergy_std = SQRT(SUM((bestEnergy_all-bestEnergy_average)**2)/SIZE(bestEnergy_all,1))
        kMax_std = SQRT(SUM((kMax_all-kMax_average)**2)/SIZE(kMax_all,1))

        write(*,*)"__NeighborMaping", whichNeighborMaping
        write(*,*)"          last energy <L_K>=", finalEnergy_average, &
                  &"          last energy sigma[ L_K ]=", finalEnergy_std
        write(*,*)"          best energy <\tilde{L}^*>=", bestEnergy_average, &
                  &"          best energy sigma[ \tilde{L}^* ]=", bestEnergy_std        
        write(*,*)"          max iteration < k_{max} >=", kMax_average, &
                  &"          max iteration sigma[ k_{max} ]=", kMax_std
        
        bestEnergy_average = SUM(bestEnergy_all)/SIZE(bestEnergy_all,1)
    END DO
write(*,*)"___Computation ended___"
END program Template_program