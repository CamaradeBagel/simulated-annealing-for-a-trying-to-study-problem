#!/bin/bash

# compile the FORTRAN code, comment when final
make
# run FORTRAN program, output in files
./Test_NeighborsMaping
# plot the data with python
python3 OpenData_test_NeighborsMaping.py
