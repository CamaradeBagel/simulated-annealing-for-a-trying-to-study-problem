# -*- coding: utf-8 -*-
"""
Created on Sat may  25 15:49:16 2019

Open the results from a TSP simulated annealing written in a .txt file by the FORTRAN optimization programm.

This aim to plot the results in a manner sensible to human vision.

@author: utilisateur1
"""

import numpy as np # datahandling
import glob # filename search

#For plotting
#from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

color = ['#436bad','#fb7d07','#019529']#,'#ff0490']
Schedule_list = [1,2,3]
every = 100 #saved data was sampled every "every" pt in the optimization history

print("Plotting energies evolution...")
# create a figure and axis to contain the energies evolutions plots
fig, ax = plt.subplots(nrows=1, ncols=1)
ax.set_xlabel(r"Temperature $T_k$", fontsize=16)
ax.set_ylabel(r"Energy $E_k=\mathcal{H}[\mathcal{S}^k]=L[\mathcal{S}^k,D]$", fontsize=16)

# create a figure and axis to contain the temperature cooling schedule plots
fig2, ax2 = plt.subplots(nrows=2, ncols=1)
ax2[0].set_xlabel(r"Iteration $k$", fontsize=16)
ax2[0].set_ylabel(r"Temperature $T_k$", fontsize=16)
ax2[1].set_xlabel(r"Iteration $k$", fontsize=16)
ax2[1].set_ylabel(r"Temperature $T_k$", fontsize=16)

for whichSchedule in Schedule_list:
    # Find a list of all the files that correspond to this cooling schedule
    Energiesfilename_all = glob.glob("energyHistory_"+str(whichSchedule)+"_*_*.txt")
    # Initialize a 2D array to contain the energy data
    Energies_data = []
    maxIter = 0 # we'll find the maximum of iteration that was done
    print("Opening data files...")
    # for every file corresponding to different test with the same cooling schedule...
    for i,Energiesfilename in enumerate(Energiesfilename_all):
        data = np.loadtxt(Energiesfilename)
        Energies_data.append(data[:,1])
        NbIter_i = np.size(data, 0) # how many iteration in this test?
        if NbIter_i > maxIter:
            maxIter = NbIter_i
            TSchedule = data[:,0] # we need the longest temperature sample
    # now that we have all data and the max number of iteration, we put everything in the same array,
    #    patching with NaN when there's less iterations
    NbTests = np.size(Energiesfilename_all,0) # number of tests made with that cooling schedule
    # initialize the array
    Energies_data_array = np.empty(( NbTests , maxIter ))
    # fill with the energies evolutions and NaN if less iteration
    for i in range(NbTests):
        NbIter = np.size(Energies_data[i],0)
        Energies_data_array[i,0:NbIter] = Energies_data[i]
        if NbIter<maxIter:
            Energies_data_array[i,NbIter:] = np.nan*np.ones((1,maxIter-NbIter))

    print("Plotting for a schedule...")
    # plot the temperature schedule
    ax2[0].plot(every*np.arange(maxIter), TSchedule, marker='.', markersize=1., linestyle='-',\
                markerfacecolor=color[whichSchedule-1], alpha=0.9)
    ax2[1].plot(every*np.arange(maxIter), TSchedule, marker='.', markersize=1., linestyle='-',\
                markerfacecolor=color[whichSchedule-1], alpha=0.9)
    # plot the energies of the multiples tests with the cooling schedule
    for i in range(NbTests):
        ax.plot(TSchedule, Energies_data_array[i,:], marker='.', markersize=1., \
                linestyle='-', linewidth=0.5, color=color[whichSchedule-1], \
                markerfacecolor=color[whichSchedule-1], markeredgecolor='none', \
                alpha=0.4, label='_nolegend_')
    # plot the average energy evolution for that cooling schedule
    Energies_data_average = np.mean(Energies_data_array, 0)
    ax.plot(TSchedule, Energies_data_average, marker='.', markersize=2., \
                linestyle='-',linewidth=1.5, color=color[whichSchedule-1], \
                markerfacecolor=color[whichSchedule-1], markeredgecolor='none', alpha=0.95)

ax.legend([r"$\mathcal{T}_{linear}$", r"$\mathcal{T}_{geometric}$", r"$\mathcal{T}_{divlog}$"],\
             fontsize=16, numpoints=2, loc='best', markerscale=5)
ax2[0].legend([r"$\mathcal{T}_{linear}$", r"$\mathcal{T}_{geometric}$", r"$\mathcal{T}_{divlog}$"],\
             fontsize=16, numpoints=2, loc='best', markerscale=5)
ax2[0].set_ylim([-1., 5.])
ax2[1].set_yscale('log')
print("Display of the plots...")
plt.show()
