module TSP_SimAnn_module
    !
    ! This module contain the functions/subroutines used to set up
    !   and approximatly solve a  Travelling Salesperson Problem (TSP) 
    !   using a simulated annealing method. 
    ! That is, we are looking to an approximate solution of what is the Hamiltonian cycle of a
    !    weighted connected graph that minimize the total weight on the cycle.
    !
    ! A problem is defined by a list of N points in a n-dimensionnal space,
    !   and the distances between each other (defining the space behavior/topology).
    !   That is two arrays: one of Nxn with row i the coordinates of point i, one of NxN
    !   where the element i,j is distance from i to j. 
    ! A non-symetric TSP can be implemented, in which the distances array is not symetric.
    ! Several subroutines are implemented to create problems in different type of spaces.
    !
    ! To (approximatly) solve by simulated annealing, we first initialize the path whitin the points (the state of
    !   the system) in a random way. We then define a rule to find a "neighbor" state of the search space of possible
    !   states, and find the difference in cost function (energy) between the current state and this new possible trial
    !   state. The proposed new trial state is accepted (and become the current state) if its energy is lower, or
    !   according to a probability distribution of energy difference and "Temperature" parameter if it increase energy
    !   (That's Metropolis algorithm). The temperature parameter is gradually lowered according to a schedule, thus
    !   limiting more and more the search to LOCALLY optimal states.
    ! Therefore, an implementation of the method requires: 
    !   - A method to find a neihbor state in search space (e.g. flipping points in the path)
    !   - A cost function (an "effective Hamiltonian") that return energy of a state in function
    !     of the state and the distances array, it may be more efficient to only compute the energy difference
    !    (which may simple due to the neighbor method)
    !   - A probability distribution of Temperature parameter and energy difference that describe the probabilities
    !     of accepting non-locally-optimal states
    !   - A schedule for the decrease of the temperature parameter (which include the initial temperature,
    !     and the list of subsequent temperatures)
    !
    ! All those different parameters/possibilities in the implementation of the TSP problem and the simulated annealing
    !   method are defined separatly to allow for modifications and comparisons.
    !__________________________________________________________________________________________________________________
    !CODE STARTS HERE
    !------------------------------------------------------------------------------------------------------------------
    !! We want to use OpenMPI, with newest Fortran2008 version of it
    !use mpi_f08
    ! implicit variables are to be banned, and rather than "God is real, unless specified to be integer" we
    ! fully assume our responsability: God exists only if we decide to.
    implicit none 
    private
    public dp, TSP_problem,&
             &simulated_annealing, randomState, flipProbDist_Gibbs, effHamil_totDist,&
             &neighborState_VertexInsert, DeltaE_totDist_vertexInsert, neighborState_switch,&
             &DeltaE_totDist_switch, DeltaE_totDist_switch_symetric, neighborState_blockReverse,&
             &DeltaE_totDist_blockReverse, DeltaE_totDist_blockReverse_symetric, &
             &neighborMethod_totDist_vertexInsert, neighborMethod_totDist_switch,&
             &neighborMethod_totDist_switch_symetric, neighborMethod_totDist_BlockReverse,&
             &neighborMethod_totDist_BlockReverse_symetric,&
             &randomPts_Euclidian, circlePts_Euclidian,&
             &linearScheduleT, divLogScheduleT, geometricScheduleT, expScheduleT,&
             &WriteResultsVec, WriteResults

    integer, parameter:: dp=kind(0.d0)   ! double precision quickly written

    type TSP_Problem
            ! Define a derived data type (a somewhat "class") to contain the data about a particular TSP
            real(dp), allocatable:: Coords(:,:) ! nXN array of coordinates of N points in n dimensions
            real(dp), allocatable:: Distances(:,:) ! NxN array of distances, element (i,j)
                                      !  is distance from pt i to pt j
    end type TSP_Problem

    CONTAINS        

        subroutine simulated_annealing(Problem, effHamil, flipProbDist, neighborMethod, scheduleT, &
                                      &finalState, finalEnergy, bestState, bestEnergy, &
                                      &historyEnergies, historyStates, initState)
            implicit none ! as usual, we would like to conserve our sanity when debugging
            ! Perfom simulated annealing approximative optimization of a TSP problem, 
            !   this is the core of the module, and makes use of many functions defined later
            ! first, state that Problem type is the "problem class" we defined before
            type(TSP_Problem) :: Problem
            
            ! second, state what the input functions/subroutines should be like in term of input/output:
            INTERFACE
                ! The IMPORT statements in the interface are required to refer to definitions made in the
                !    scope of the module to which we interface
                real(dp) function effHamil(State, Distances)
                    IMPORT dp
                    integer, intent(in):: State(:)
                    real(dp), intent(in):: Distances(:,:)
                    ! depends on the state of the system and what is the list of distances between pts/their space
                end function effHamil

                real(dp) function flipProbDist(deltaE, T)
                    IMPORT dp
                    real(dp), intent(in):: deltaE, T
                    ! Depends on effective energies difference (final-initial) and temperature
                end function flipProbDist

                subroutine neighborMethod(Problem, State, trialState, deltaE)
                    IMPORT TSP_problem, dp
                    type(TSP_Problem), intent(in):: Problem
                    integer, intent(in):: State(:)
                    integer, intent(out):: trialState(:)
                    real(dp), intent(out):: deltaE
                    ! Depends on current state, return trial state and deltaE
                end subroutine neighborMethod

            END INTERFACE

            ! third, initialize the other input/output:
            ! an array of floats with the temperatures for each iterations
            real(dp), intent(in):: scheduleT(:)
            ! the last state that was computed, an array of integers, 
            ! and the one with the lowest cost encountered during annealing
            integer, intent(out):: finalState(:), bestState(:)
            ! also the corresponding costs/Energies
            real(dp), intent(out):: finalEnergy, bestEnergy
            ! maybe we want to save the evolution of the process (Paths and energies)
            real(dp), intent(out), optional, allocatable:: historyEnergies(:)
            integer, intent(out), optional, allocatable::  historyStates(:,:)
            ! maybe we want to specify a fixed initial trial path,
            ! to compare different evolutions from it (not random initial state)
            integer, intent(in), optional:: initState(:)

            ! last, we define the local variables that are only used within the simulated annealing:
            integer:: N_pts, n_dim, K_step, k, stable_count ! number of points, dimension of the space of the pts,
                                                            !    number of iterations, iteration loop counter,
                                                            !   stable iterations counter                                     
            integer, parameter:: KstableIter=300 ! if the solution havent changed in KstableIter-> break
            real(dp):: T, deltaE, r, Energy ! Current temperature, effective energy difference of
                                                         !   a trial, random number, current Energy
            integer, allocatable:: State(:), trialState(:) ! Current state of the system, trial state of a Metropolis step
            real(dp), allocatable:: historyEnergiesTemporary(:) ! We will save in those, and output the non-zero elements
            integer, allocatable::  historyStatesTemporary(:,:) !    of those at the end
            ! we check the size of the "Problem" input and define/verify the size of the other variable
            ! accordingly
            n_dim = Size(Problem%Coords, 1) ! Number of dimensions
            N_pts = Size(Problem%Coords, 2) ! Number of points
            ALLOCATE(State(N_pts)) ! list of the pts index in the order they are visited, hence, N_pts elements
            ALLOCATE(trialState(N_pts))
            ! check size of the temperature schedule, that is the maximal number of iterations
            K_step = Size(scheduleT)
            ALLOCATE(historyStatesTemporary(N_pts, K_step))
            ALLOCATE(historyEnergiesTemporary(K_step))

            IF ((N_pts /= Size(Problem%Distances, 1)) .OR. (N_pts /= Size(Problem%Distances, 2))) THEN
                ! the problem is ill defined, the sizes of the coordinates list and the distances matrix doesn't match
                WRITE(*,*) 'The TSP problem given as input argument is inconsistent, Distances and Coordinates doesn''t&
                             &have the same number of points'
                STOP ! end program
            END IF

            ! if initial state is given, check it is coherent, if not, initialize randomly
            IF (PRESENT(initState)) THEN
                ! given initial state should also have lenght N (N-1 steps + 1 initial point)
                IF (N_pts /= Size(initState, 1))  THEN
                    ! the initial state doesn't match the size of the problem
                    WRITE(*,*) 'Given initial state doesn''t match the problem size.'
                    STOP ! end program
                END IF
                ! if the state is ok, set up as the initial state
                State = initState
            ! if no given initial state, then initialize randomly *to start obviously not an optimal solution*
            ELSE 
                State = randomState(N_pts) ! function defined later
            END IF

            ! Also initialize energy
            Energy = effHamil(State, Problem%Distances)

            ! Even if the initial state may be super non-optimized, it is the best until now
            bestState = State
            bestEnergy = Energy

            ! We want a new random seed for each computation/process, so
            CALL initRandom()

            ! initially, no successive iteration have resulted in no change
            stable_count = 0

            WRITE(*,*) "    The simulated annealing starts."
            ! We loop over the temperature schedule
            DO k=1, K_step
                T = scheduleT(k) ! current Temperature from the schedule
                ! produce trial next State as a "neighboring" solution and compute energy difference
                ! This is done by the neighborMethod subroutine given as an argument of the solver
                CALL neighborMethod(Problem, State, trialState, deltaE)

                ! we sample with Metropolis algorithm,
                !    probability of accepting locally non-optimal solutions should decrease with T
                CALL RANDOM_NUMBER(r) ! random number between 0 and 1
                IF (r .LE. flipProbDist(deltaE, T) ) THEN
                    ! The trial is accepted, 
                    State = trialState
                    Energy = Energy + deltaE
                    ! there have been a change, the stability of the approximative solution isn't established
                    stable_count = 0
                    ! also, if changed state, should check if we have found an overall better solution
                    IF (Energy.LT.bestEnergy) THEN
                        bestEnergy = Energy
                        bestState = State
                    END IF
                ELSE 
                    ! If the proposed change was not accepted, we update the stability counter
                    stable_count = stable_count + 1
                END IF

                ! save the current state if required
                IF (PRESENT(historyStates)) THEN 
                    historyStatesTemporary(:,k) = State
                END if
                ! save the current energy if required
                IF (PRESENT(historyEnergies)) THEN 
                    historyEnergiesTemporary(k) = Energy
                END if
                
                ! If the optimization haven't resulted in any change for too long, probably the approximative solution
                !    is now stable/converged, so we stop to iterate
                IF (stable_count .GE. KstableIter) THEN
                    WRITE(*,*)"Convergence before the end of the schedule"
                    EXIT ! the DO k=.. loop
                END IF

            END DO
            
            ! Now the approximated solution should have been found, and the TSP simulated annealing is finished
            finalEnergy = Energy 
            finalState = State
            ! If history output is required, we output only the computed iteration results, not the empty elements
            IF (PRESENT(historyStates)) THEN
                ALLOCATE(historyStates(N_pts, k-1))
                historyStates(:,:) = historyStatesTemporary(:,:k-1)
            END IF
            IF (PRESENT(historyEnergies)) THEN
                ALLOCATE(historyEnergies(k-1))
                historyEnergies(:) = historyEnergiesTemporary(:k-1)
            END IF
            WRITE(*,*) "    The simulated annealing is completed."
        end subroutine simulated_annealing
        
        !______________________________________________________________________________________________________
        ! functions that are used in the simulated annealing subroutine

        subroutine initRandom()
            implicit none
            integer :: idate(8), iseed(12)
            ! This subroutine initialized a new random seed from the current time.
            ! It should be called in the beginning of any parallel process to ensure
            ! there's no correlation in the pseudo-random number generators.
            !
            ! Initialization of rng by time:
            CALL date_and_time(VALUES=idate)
            !write(*,*) 'date and time is ', idate
            iseed(1:12) = idate(7)*1000+idate(8)
            !write(*,*) "seed generated is ", iseed
            CALL random_seed(put=iseed)
            !write(*,*) 'The pseudo random generator seed is ', iseed
        end subroutine initRandom

        function randomState(N_pts) result(State0)
            ! Creates a random path (loop) between N points (for initialization).
            ! A state/path is encoded as a list of N indexes.
            ! The indexes in State(1:N) encode the order in which th path visits the points
            ! we use Knuth shuffling to create the random shuffling of the N pts.
            implicit none ! as usual, we would like to conserve our sanity when debugging
            integer, intent(in):: N_pts
            integer:: State0(N_pts)
            integer:: i,j, k, Sj, Sk ! index for loop, index for shuffle position, index for position to exchange,
                                    !   points in positions j and k
            real(dp):: R ! random number
            ! initialize with a list of order integer from 1 to N_pts
            State0 = (/(i, i=1,N_pts)/)
            ! we shuffle with Knuth (Fisher–Yates) shuffle
            DO i=0,(N_pts-1)
                j = N_pts - i
                CALL RANDOM_NUMBER(R) ! random real R\in[0,1]
                k = FLOOR(j*R)+1 ! random integer k\in {1,...,j}
                ! exchange the points that are in the j^th and k^th position
                Sj = State0(j)
                Sk = State0(k)
                State0(j) = Sk 
                State0(k) = Sj
            END DO

        end function randomState

        !__________________________________________________________________________________
        ! Now define functions that gives the cases of effective hamiltonian/cost and probability distribution of the
        ! trial state acceptancy that are tested in the scope of this project

        function flipProbDist_Gibbs(deltaE, T) result(prob)
            ! probability distribution of sampled state,
            !    probability of accepting a trial that imply deltaE in energy
            implicit none
            real(dp):: prob
            real(dp), intent(in):: deltaE, T 
            ! We use a simple Gibbs distribution
            !   (the exp is computationaly heavy, but that's a first try)
            prob = EXP( (-1._dp*deltaE)/T )
        end function flipProbDist_Gibbs


        function effHamil_totDist(State, Distances) result(E)
            ! effective Hamiltonian of a state, that return the energy/cost
            ! of a state, that is a path between N pts, given the distances/cost
            ! between the points
            ! Here the total energy is simply the total distance
            implicit none
            integer, intent(in):: State(:)
            real(dp), intent(in):: Distances(:,:)
            real(dp):: E
            integer:: N_pts, i ! number of points to join in the path, index in loop,
                                   ! reduced indexes that account for periodicity
            N_pts = SIZE(State, 1)
            E = 0 ! initialize

            DO i=1,N_pts-1
                E = E + Distances(State(i), State(i+1))
            END DO
            E = E + Distances(State(N_pts), State(1)) ! periodicity of the path between pts,
                                                      ! last jump from N^th pt of the state to first
        end function effHamil_totDist

        !________________________________________________________________________________
        ! Some neighbor_method subroutine to use for the simulated annealing subroutine,
        ! including a function to find a neighbor state, a function to find the variation
        ! in energy and a subroutine that call both of them

        function neighborState_VertexInsert(i, j, State) result(trialState)
            ! find and propose a neighboring state in the search space for the solution,
            !    by taking the ith pt in the path and placing it after the jth pt
            implicit none
            integer, allocatable:: trialState(:)
            integer, intent(in):: State(:), i, j
            integer:: N_pts ! number of points in the path
            N_pts = SIZE(State)
            ALLOCATE(trialState(N_pts))
            IF (i.LT.j) THEN ! i<j
                trialState(:i-1) = State(:i-1) ! same
                trialState(i:j-1) = State(i+1:j) ! removed i^th
                trialState(j) = State(i) ! insert i^th after initial j^th
                trialState(j+1:N_pts) = State(j+1:) ! same
            ELSE ! that is j<i
                trialState(:j-1) = State(:j-1) ! same
                trialState(j:i-1) = State(j+1:i) ! removed j^th
                trialState(i) = State(j) ! insert j^th after initial i^th
                trialState(i+1:N_pts) = State(i+1:) ! same
            END IF
        end function neighborState_VertexInsert


        function DeltaE_totDist_vertexInsert(i,j, initState, Distances) result(deltaE)
            ! Energy difference in a somewhat specific case: the energy is defined as the total
            ! distance and the flip corresponds to removing i^th pt of the path and placing it after j^th pt
            implicit none
            integer, intent(in):: i, j, initState(:)
            real(dp), intent(in):: Distances(:,:)
            real(dp):: deltaE
            integer::N_pts, a, a_, a1, b, b1 ! Number of pts, identification of the pts that are at positions
            ! i, j and around in the path
            ! The links between (in order of the initial path) i->i+1 , i-1->i and j->j+1 are broken,
            !    links between i-1->i+1 , j->i and i->j+1 are created
            ! identify the point index of each of those
            ! we use modulo to account for when i\in {1, N} or j\in{1,N} and we need to force periodicity of the path
            N_pts = SIZE(initState,1)
            a = initState( i )
            a_ = initState( MODULO(i-2, N_pts)+1 ) ! (i-1) - 1 = i-2
            a1 = initState( MODULO(i, N_pts)+1 ) ! (i+1) - 1 = i
            b = initState( j )
            b1 = initState( MODULO(j, N_pts)+1 ) !(j+1)-1 = j
            
            ! difference in energy between final and initial state
            deltaE = ( Distances(a_,a1) + Distances(b,a) + Distances(a, b1) ) 
            deltaE = deltaE - ( Distances(a_,a) + Distances(a,a1) + Distances(b,b1) )

        end function DeltaE_totDist_vertexInsert

        subroutine neighborMethod_totDist_vertexInsert(Problem, State, trialState, deltaE)
            ! return a neighboring solution from the current state using random vertex insert.
            ! That is, a random pt in the path is removed and inserted at another random position.
            ! The trial state and the resulting energy/distance/cost difference is returned.
            ! We assume total energy is sum of distances.
            implicit none
            type(TSP_Problem), intent(in):: Problem
            integer, intent(in):: State(:) ! current state
            integer, intent(out):: trialState(:) ! returned trial state (after vertex insert)
            real(dp), intent(out):: deltaE ! difference in energy trialEnergy - Current energy
            real(dp):: R(2) ! random real numbers for producing integers
            integer:: N_pts, i,j ! Number of points, random indexes in path of the removed pt (i),
                                 !    and random insertion position (j)
            ! we want two pseudo-random integers that are uniformly distributed between 1 and N_pts
            N_pts = SIZE(State) ! find size of path, number of points
            CALL RANDOM_NUMBER(R) ! create pseudo-random reals between 0 and 1
            i = FLOOR((N_pts - 1)*R(1)) + 1 ! random integer between 1 and N_pts from r1
            j = FLOOR((N_pts - i-1)*R(2)) + 1 + i ! random integer between i+1 and N_pts from r2
            trialState = neighborState_VertexInsert(i, j, State)
            deltaE = DeltaE_totDist_vertexInsert(i, j, State, Problem%Distances)
        end subroutine neighborMethod_totDist_vertexInsert
        ! ------------------------------------------------------------------------------------------------


        function neighborState_switch(i, State) result(trialState)
            ! find and propose a neighboring state in the search space for the solution,
            !    by taking the ith pt in the path and switching it with the i+1^th
            implicit none
            integer, allocatable:: trialState(:)
            integer, intent(in):: State(:), i
            integer:: N_pts, i1 !number of points in the path
            N_pts = SIZE(State, 1)
            ALLOCATE(trialState(N_pts)) ! trial state has the same size and index as State
            trialState = State ! initially similar
            i1 = MODULO(i, N_pts)+1  ! (i+1) - 1 = i, and we loop to i+1 -> 1 if i=N
            trialState(i) = State(i1) ! exchange i^th and i+1^th pts
            trialState(i1) = State(i)
        end function neighborState_switch

        function DeltaE_totDist_switch(i, initState, Distances) result(deltaE)
            ! Energy difference in a somewhat specific case: the energy is defined as the total
            ! distance and the flip corresponds to switch i^th and i+1^th pts in the path
            implicit none
            integer, intent(in):: i, initState(:)
            real(dp), intent(in):: Distances(:,:)
            real(dp):: deltaE
            integer:: N_pts,  a, a_, a1, a2 ! total number of pts, points identification from the list in the Path
            ! links between (in order of the initial path) i-1->i , i->i+1 , i+1->i+2 are broken,
            !    links between i-1 -> i+1 , i+1 ->i and i -> i+2 are created
            ! identify the point index of each of those, with periodicity of the path for -1,+1,+2
            N_pts = SIZE(initState,1)
            a = initState( i )
            a_ = initState( MODULO(i-2, N_pts)+1 ) ! (i-1) - 1 = i-2
            a1 = initState( MODULO(i, N_pts)+1 ) ! (i+1) - 1 = i
            a2 = initState( MODULO(i+1, N_pts)+1 ) ! (i+2) - 1 = i + 1
            ! difference in energy between final and initial state
            deltaE = Distances(a_,a1) + Distances(a1,a) + Distances(a,a2) &
                        &- ( Distances(a,a1) + Distances(a_,a) + Distances(a1,a2) )
        end function DeltaE_totDist_switch

        function DeltaE_totDist_switch_symetric(i, initState, Distances) result(deltaE)
            ! Energy difference in a somewhat specific case: the energy is defined as the total
            ! distance, the flip corresponds to switch i^th and i+1^th pts in the path, and the distances are symetric
            implicit none
            integer, intent(in):: i, initState(:)
            real(dp), intent(in):: Distances(:,:)
            real(dp):: deltaE
            integer:: N_pts,  a, a_, a1, a2 ! total number of pts, points identification from the list in the Path
            ! links between (in order of the initial path) i-1->i , i->i+1 , i+1->i+2 are broken,
            !    links between i-1 -> i+1 , i+1 ->i and i -> i+2 are created (and i-1->i = i->i-1)
            ! identify the point index of each of those, with periodicity of the path for -1,+1,+2
            N_pts = SIZE(initState,1)
            a = initState( i )
            a_ = initState( MODULO(i-2, N_pts)+1 ) ! (i-1) - 1 = i-2
            a1 = initState( MODULO(i, N_pts)+1 ) ! (i+1) - 1 = i
            a2 = initState( MODULO(i+1, N_pts)+1 ) ! (i+2) - 1 = i + 1
            ! difference in energy between final and initial state
            deltaE = Distances(a_,a1) + Distances(a,a2) &
                        &- ( Distances(a_,a) + Distances(a1,a2) )
        end function DeltaE_totDist_switch_symetric

        

        subroutine neighborMethod_totDist_switch(Problem, State, trialState, deltaE)
            ! return a neighboring solution from the current state using random switch.
            ! That is, a random pt position in the path is switched with the next one in the path.
            ! The trial state and the resulting energy/distance/cost difference is returned.
            ! We assume total energy is sum of distances.
            implicit none
            type(TSP_Problem), intent(in):: Problem
            integer, intent(in):: State(:) ! current state
            integer, intent(out):: trialState(:) ! returned trial state (after vertex insert)
            real(dp), intent(out):: deltaE ! difference in energy trialEnergy - Current energy
            real(dp):: r1 ! random real for producing integers
            integer:: N_pts, i ! Number of points, random indexes in path of the switched pt (i)
            ! we want a pseudo-random integer that are uniformly distributed between 1 and N_pts
            N_pts = SIZE(State) ! find size of path, number of points
            CALL RANDOM_NUMBER(r1) ! create pseudo-random real between 0 and 1
            i = FLOOR((N_pts - 1)*r1) + 1 ! random integer between 1 and N_pts from r1
            trialState = neighborState_switch(i, State)
            deltaE = DeltaE_totDist_switch(i, State, Problem%Distances)
        end subroutine neighborMethod_totDist_switch

        subroutine neighborMethod_totDist_switch_symetric(Problem, State, trialState, deltaE)
            ! return a neighboring solution from the current state using random switch.
            ! That is, a random pt position in the path is switched with the next one in the path.
            ! The trial state and the resulting energy/distance/cost difference is returned.
            ! We assume total energy is sum of distances.
            ! We assume the distances are symetric, Distances(i,j)=Distances(j,i)
            implicit none
            type(TSP_Problem), intent(in):: Problem
            integer, intent(in):: State(:) ! current state
            integer, intent(out):: trialState(:) ! returned trial state (after vertex insert)
            real(dp), intent(out):: deltaE ! difference in energy trialEnergy - Current energy
            real(dp):: r1 ! random real for producing integers
            integer:: N_pts, i ! Number of points, random indexes in path of the switched pt (i)
            ! we want a pseudo-random integer that are uniformly distributed between 1 and N_pts
            N_pts = SIZE(State) ! find size of path, number of points
            CALL RANDOM_NUMBER(r1) ! create pseudo-random real between 0 and 1
            i = FLOOR((N_pts - 1)*r1) + 1 ! random integer between 1 and N_pts from r1
            trialState = neighborState_switch(i, State)
            deltaE = DeltaE_totDist_switch_symetric(i, State, Problem%Distances)
        end subroutine neighborMethod_totDist_switch_symetric
        ! ------------------------------------------------------------------------------------------------


        function neighborState_blockReverse(i, j, State) result(trialState)
            ! find and propose a neighboring state in the search space for the solution,
            !    by inverting the path between the i^th and j^th pts (i and j included)
            implicit none
            integer, allocatable:: trialState(:)
            integer, intent(in):: State(:), i, j
            integer:: N_pts, l ! number of points in the path, index over lenght of the flipped block
            N_pts = SIZE(State)
            ALLOCATE(trialState(N_pts))
            
            trialState(:i-1) = State(:i-1) ! same before the reversed block
            DO l=0,(ABS(j-i)) ! for the state within the flipped block, we have index l
                trialState(i+l) = State(j-l) ! we reverse 1 by 1, taking j-l pt to put it in position i+l
            END DO
            trialState(j+1:) = State(j+1:) ! same  after the reversed block       
            
        end function neighborState_blockReverse


        function DeltaE_totDist_blockReverse(i,j, initState, Distances) result(deltaE)
            ! Energy difference in a somewhat specific case: the energy is defined as the total
            ! distance and the flip corresponds to inverting the path between the i^th and j^th pts
            implicit none
            integer, intent(in):: i, j, initState(:)
            real(dp), intent(in):: Distances(:,:)
            real(dp):: deltaE
            integer:: N_pts, l, a, b ! lenght of the flipped block, labels of the pts in the distance array/Coord list
            ! links between (in order of the initial path) i and j are inverted
            deltaE = 0._dp ! initialization
            ! sum over the block, where links are inverted /totally changed on the boundaries
            ! we use modulo to force periodicity if i-1, i-2 or j+1 is <1 or >N
            N_pts = SIZE(initState,1)
            DO l=0,(j-i+1)
                a = initState( MODULO(i-2+l, N_pts)+1 ) ! (i-1+l) - 1 = i-2+l
                b = initState( MODULO(i-1+l, N_pts)+1 ) ! (i+l) - 1 = i-1+l
                deltaE = deltaE - Distances(a, b)
            END DO

            a = initState( MODULO(i-2, N_pts)+1 ) ! (i-1+l) - 1 = i-2+l
            b = initState( j ) ! (i+l) - 1 = i-1+l
            deltaE = deltaE + Distances(a,b)
            a = initState( i ) ! (i-1+l) - 1 = i-2+l
            b = initState( MODULO(j, N_pts)+1 ) ! (i+l) - 1 = i-1+l
            deltaE = deltaE + Distances(a,b)

            DO l=0,(j-i)
                a = initState( MODULO(j-1-l, N_pts)+1 ) ! (j-l) - 1 = j-1-l
                b = initState( MODULO(j-2-l, N_pts)+1 ) ! (j-1-l) - 1 = j-2-l
                deltaE = deltaE + Distances(a,b)
            END DO
            
        end function DeltaE_totDist_blockReverse

        function DeltaE_totDist_blockReverse_symetric(i,j, initState, Distances) result(deltaE)
            ! Energy difference in a somewhat specific case: the energy is defined as the total
            ! distance, the flip corresponds to inverting the path between the i^th and j^th pts and the 
            ! distances are symetric
            implicit none
            integer, intent(in):: i, j, initState(:)
            real(dp), intent(in):: Distances(:,:)
            real(dp):: deltaE
            integer:: N_pts, a, a_, b, b1 ! labels of the pts in the distance array/Coord list
            ! links between (in order of the initial path) i and j are inverted
            ! only the boundaries changes impact energy
            ! we use modulo to force periodicity if i-1, i-2 or j+1 is <1 or >N
            N_pts = SIZE(initState,1)
            a = initState(i)
            a_ = initState( MODULO(i-2,N_pts)+1 ) ! (i-1)-1 = i-2
            b = initState(j)
            b1 = initState( MODULO(j,N_pts)+1 ) ! (j+1)-1 = j
            deltaE = Distances(a_,b) + Distances(a,b1) - ( Distances(a_,a) + Distances(b,b1) )
            
        end function DeltaE_totDist_blockReverse_symetric


        subroutine neighborMethod_totDist_BlockReverse(Problem, State, trialState, deltaE)
            ! return a neighboring solution from the current state using block reversal.
            ! That is, a block of pts from the i^th in the path to the j^th in the path are reversed
            ! (e.g. from i to j rather than j to i)
            ! The trial state and the resulting energy/distance/cost difference is returned.
            ! We assume total energy is sum of distances.
            implicit none
            type(TSP_Problem), intent(in):: Problem
            integer, intent(in):: State(:) ! current state
            integer, intent(out):: trialState(:) ! returned trial state (after vertex insert)
            real(dp), intent(out):: deltaE ! difference in energy trialEnergy - Current energy
            real(dp):: R(2) ! random real for producing integers
            integer:: N_pts, i,j ! Number of points, random indexes in path of the first pt (i),
                                !    and last pt (j)
            ! we want two pseudo-random integers that are uniformly distributed between 1 and N_pts
            N_pts = SIZE(State,1) ! find size of path, number of points
            CALL RANDOM_NUMBER(R) ! create pseudo-random real between 0 and 1
            i = FLOOR((N_pts - 1)*R(1)) + 1 ! random integer between 1 and N_pts from r1
            j = FLOOR((N_pts - i - 1)*R(2)) + i+1 ! random integer between i+1 and N_pts from r2
            trialState = neighborState_blockReverse(i, j, State)
            deltaE = DeltaE_totDist_blockReverse(i,j, State, Problem%Distances)
        end subroutine neighborMethod_totDist_BlockReverse

        subroutine neighborMethod_totDist_BlockReverse_symetric(Problem, State, trialState, deltaE)
            ! return a neighboring solution from the current state using block reversal.
            ! That is, a block of pts from the i^th in the path to the j^th in the path are reversed
            ! (e.g. from i to j rather than j to i)
            ! The trial state and the resulting energy/distance/cost difference is returned.
            ! We assume total energy is sum of distances.
            ! We assume the distances are symetric, Distances(i,j)=Distances(j,i)
            implicit none
            type(TSP_Problem), intent(in):: Problem
            integer, intent(in):: State(:) ! current state
            integer, intent(out):: trialState(:) ! returned trial state (after vertex insert)
            real(dp), intent(out):: deltaE ! difference in energy trialEnergy - Current energy
            real(dp):: R(2) ! random real for producing integers
            integer:: N_pts, i,j ! Number of points, random indexes in path of the first pt (i),
                                !    and last pt (j)
            ! we want two pseudo-random integers that are uniformly distributed between 1 and N_pts
            N_pts = SIZE(State,1) ! find size of path, number of points
            CALL RANDOM_NUMBER(R) ! create pseudo-random real between 0 and 1
            i = FLOOR((N_pts - 1)*R(1)) + 1 ! random integer between 1 and N_pts from r1
            j = FLOOR((N_pts - i - 1)*R(2)) + i + 1 ! random integer between i+1 and N_pts from r2
            trialState = neighborState_blockReverse(i, j, State)
            deltaE = DeltaE_totDist_blockReverse_symetric(i,j, State, Problem%Distances)
        end subroutine neighborMethod_totDist_BlockReverse_symetric

!________________________________________________________________________________
        ! The functions that actually implement simulated annealing of the TSP are defined,
        ! We now define functions that create different TS Problems settings (TSP_Problem objects)
        !   and different cooling schedules.

        function randomPts_Euclidian(N_pts ,n_dim, d) result(Problem)
            ! Create a TSP with N points randomly distributed in a dxdx...xd box in a n dimensionnal Euclidian space
            ! Therefore, the distance is simply the Euclidian distance between the points, and the distance array
            !    is symetric. (there's also no boundary/minimal image conditions)
            implicit none
            type(TSP_Problem):: Problem ! TSP which contain pts and their distances
            integer, intent(in):: N_pts, n_dim ! number of pts, number of dimensions
            real(dp), intent(in):: d ! size of the side of the (hyper)-cubic Euclidian box
            integer:: i, j ! loop indexes
            ! First, create the points
            ! create N random uniformly distributed coordinates in [0,1] for every dimension in R^n,
            !    with correct indexing (defining the size of Coords)
            ALLOCATE(Problem%Coords(1:n_dim, 1:N_pts))
            ALLOCATE(Problem%Distances(1:N_pts, 1:N_pts))
            CALL initRandom()
            CALL RANDOM_NUMBER( Problem%Coords )
            ! expand the distribution in each dimension to [0,d]
            Problem%Coords(1:n_dim, 1:N_pts) = d*Problem%Coords(1:n_dim, 1:N_pts)
            ! Second, compute the distances
            DO j=1,N_pts
                DO i=j, N_pts 
                    ! Euclidian distance is Euclidean vector norm (L_2 norm), and already exists in the GNU compiler
                    Problem%Distances(i,j) = NORM2(Problem%Coords(:,j) - Problem%Coords(:,i))
                    ! with that distance, symetric
                    Problem%Distances(j,i) = Problem%Distances(i,j)
                END DO 
            END DO
        end function randomPts_Euclidian

        function circlePts_Euclidian(N_pts ,n_dim, R) result(Problem)
            ! Create a TSP with N points evenly placed on a radius R circle in a n dimensionnal Euclidian space.
            ! The purpose of this is to test the efficiency of the approximate solution by checking a super simple
            !    case. Therefore, the distance is simply the Euclidian distance between the points,
            !    and the distance array is symetric. (there's also no boundary/minimal image conditions)
            implicit none
            type(TSP_Problem):: Problem ! TSP which contain pts and their distances
            integer, intent(in):: N_pts, n_dim ! number of pts, number of dimensions
            real(dp), intent(in):: R ! Radius of the circle
            integer:: i, j ! loop indexes
            real(dp):: Theta, deltaTheta ! polar axis on the circle
            real(dp), parameter:: PI=4._dp*DATAN(1._dp) ! high precision pi constant
            ALLOCATE(Problem%Coords(1:n_dim, 1:N_pts))
            ALLOCATE(Problem%Distances(1:N_pts, 1:N_pts))
            ! First, create the points
            deltaTheta = (2._dp*PI)/N_pts ! equal polar angle between the points relatively to center of the circle
                                          ! the i^th pt is at angular position (i-1)*deltaTheta
            DO i=1, N_pts
                Theta = i*deltaTheta
                Problem%Coords(1,i) = R*COS(Theta) ! polar coordinates
                Problem%Coords(2,i) = R*SIN(Theta)
                ! the other dimensions coordinates are set to 0, meaning that the circle is always
                ! oriented in (0,0,1,0...,0) for simplicity
                IF (n_dim.GT.2) THEN
                    Problem%Coords(3:n_dim,i) = 0
                END IF
            END DO
            ! Second, compute the distances
            DO i=1,N_pts
                DO j=i,N_pts
                    ! Euclidian distance is Euclidean vector norm (L_2 norm), and already exists in the GNU compiler
                    Problem%Distances(i,j) = NORM2(Problem%Coords(:,j) - Problem%Coords(:,i))
                    ! the euclidian distance is symetric
                    Problem%Distances(j,i) = Problem%Distances(i,j)
                END DO 
            END DO
        end function circlePts_Euclidian

        !------------------------------------------------------------------------------------------------

        function linearScheduleT(T0, Tf, K_step) result(scheduleT)
            implicit none
            ! create a linear cooling schedule, of K evenly spaced temperatures between
            !    T0 (initial) and Tf(final).
            ! This is probably the most inneficient cooling ever.
            real(dp), intent(in):: T0, Tf ! initial temperature, final temperature
            integer, intent(in):: K_step ! number of cooling steps
            real(dp):: scheduleT(K_step) ! linear cooling schedule
            real(dp):: deltaT ! temperature step
            integer:: k ! loop index
            ! the step is constant
            deltaT = (Tf-T0)/K_step 
            ! we loop
            DO k=1, K_step
                scheduleT(k) = ((k-1)*deltaT) + T0
            END DO
        END function linearScheduleT

        function geometricScheduleT(T0, Tf, K_step) result(scheduleT)
            implicit none
            ! create a geometric cooling schedule, 
            ! T_k = (alpha)^(k-1) * T0
            ! alpha = (TF/T0)^(1/(K-1))
            real(dp), intent(in):: T0, Tf ! initial temperature, step factor
            integer, intent(in):: K_step ! number of cooling steps
            real(dp):: scheduleT(K_step) ! linear cooling schedule
            integer:: k ! loop index
            real(dp):: alpha ! parameter of the geometric decay
            alpha = (Tf/T0)**(1._dp/(K_step-1))
            ! we loop
            DO k=1, K_step
                scheduleT(k) = (alpha**(k-1))*T0
            END DO
        END function geometricScheduleT

        function expScheduleT(T0, Tf, K_step) result(scheduleT)
            implicit none
            ! create an exponential decay cooling schedule, 
            ! T_k = exp(-beta*(k-1)) * T0
            ! beta = (-1/(k-1)) * ln(Tf/T0)
            real(dp), intent(in):: T0, Tf ! initial temperature, exp factor
            integer, intent(in):: K_step ! number of cooling steps
            real(dp):: scheduleT(K_step) ! exp cooling schedule
            integer:: k ! loop index
            real(dp):: beta ! parameter of exponential decay
            beta = ((-1.0_dp)/(K_step - 1._dp))*LOG(Tf/T0)
            ! we loop
            DO k=1, K_step
                scheduleT(k) = T0*EXP(-1.0_dp*beta*(k - 1.0_dp))
            END DO
        END function expScheduleT

       function divLogScheduleT(T0, Tf, K_step) result(scheduleT)
            implicit none
            ! create an "divided by log" decay cooling schedule, 
            ! T_k = (1/log_gamma (k-1)) * T0
            ! gamma = (K-1)^(Tf/T0)
            ! inspired by "D. Bertsimas and J. Tsitsiklis, Simulated Annealing, Statistical Science, 1993, vol 8 no 1"
            real(dp), intent(in):: T0, Tf ! initial temperature, parameter
            integer, intent(in):: K_step ! number of cooling steps
            real(dp):: scheduleT(K_step) ! divlog cooling schedule
            integer:: k ! loop index
            real(dp):: gamma, log_gamma ! parameter of the divided by log decay
            gamma = (K_step)**(1._dp/( 1._dp + T0/Tf))
            ! we loop
            log_gamma = LOG(gamma)
            DO k=1, K_step
                scheduleT(k) = T0/(1._dp + LOG(k - 1._dp)/log_gamma)
            END DO
        END function divLogScheduleT

        !____________________________________________________________________________________________________
        ! define a subroutine that write the Results in different files in a format suited for reading and
        ! plotting with python
        subroutine WriteResults(Results, filename, skip, label)
            implicit none
            real(kind(1.d0)), intent(in) :: Results(:) ! The scalar results to be saved (e.g. energies ),
            real(kind(1.d0)), intent(in), optional :: label(:) ! The label/axis associated to the results (e.g. Temperature)
            character(len=100), intent(in) :: filename ! The name of the file where we write the results
            integer, intent(in), optional :: skip ! we write only 1/skip of the data to make display easier
            integer :: k, K_step, K_step_save ! index, the number of results, the number of results to save
            K_step = SIZE(Results)
            K_step_save = FLOOR((1._dp*K_step) / (1._dp*skip))
            OPEN(123,file=filename)
            if (PRESENT(label)) Then
                DO k = 1, K_step_save
                    WRITE(123,*) (/ label(k*skip), Results(k*skip) /)
                END DO
            ELSE
                DO k = 1, K_step_save
                    WRITE(123,*) (/ Results(k*skip) /)
                END DO
            END IF
            CLOSE(123)
        end subroutine WriteResults

        subroutine WriteResultsVec(Results, filename, skip, label)
            implicit none
            integer, intent(in) :: Results(:,:) ! The vector results to be saved (e.g. States ),
            real(kind(1.d0)), intent(in), optional :: label(:) ! The label/axis associated to the results (e.g. Temperature)
            character(len=100), intent(in) :: filename ! The name of the file where we write the results
            integer, intent(in), optional :: skip ! we write only 1/skip of the data to make display easier
            integer :: k, K_step, K_step_save ! index, the number of results, the number of results to save
            OPEN(123,file=filename)
            K_step = SIZE(Results, 2)
            K_step_save = FLOOR((1._dp*K_step) / (1._dp*skip))
            if (PRESENT(label)) Then
                DO k = 1, K_step_save
                    WRITE(123,*) label(k*skip), Results(:,k*skip)
                END DO
            ELSE
                DO k = 1, K_step_save
                    WRITE(123,*) Results(:,k*skip)
                END DO
            END IF
            CLOSE(123)
          end subroutine WriteResultsVec

    end module TSP_SimAnn_module
    